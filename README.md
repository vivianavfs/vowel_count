# vowel_count

## Descrição
Api que conta vogais

## Como rodar

### Instalar o python3 
Link[https://www.python.org/downloads/]

### Criar e ativar o venv (virtual enviroment):

#### Criando:
```
$python -m venv venv
```

#### Ativando:
(linux)
```
$source venv/bin/activate
```
(windows)
```
> venv\Scripts\activate
```

### Instalar o Flask e o pytest:
```
$pip install -r requirements.txt
```

#### verificando os pacotes instalados:
```
$pip freeze
```

### Rodando
```
$ python app/api.py
```

## Testes
No terminal:
```
$pytest -v
```
